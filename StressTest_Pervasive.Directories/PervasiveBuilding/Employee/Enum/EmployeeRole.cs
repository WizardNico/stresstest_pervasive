﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StressTest_Pervasive.Directories.PervasiveBuilding.Employee.Enum
{
   public enum EmployeeRole
   {
      Programmer = 10,
      Architect = 20,
      ProjectManager = 30,
      SystemEngineer = 40,
      Security = 50,
   }
}
