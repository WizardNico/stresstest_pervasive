﻿using AgileMQ.Bus;
using AgileMQ.Interfaces;
using StressTest_Pervasive.Directories.PervasiveBuilding.Employee.Models;
using StressTest_Pervasive.Directories.PervasiveBuilding.Temperature;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace StressTest_Pervasive
{
   class Program
   {
      static void Main(string[] args)
      {
         using (IAgileBus Bus = new RabbitMQBus("HostName=localhost;Port=5672;UserName=guest;Password=guest;Timeout=5;RetryLimit=3;PrefetchCount=1;AppId=Stress-Test"))
         {
            Console.WriteLine("StressTest Start....");

            List<Task<Temperature>> taskList1 = new List<Task<Temperature>>();
            List<Task<List<Temperature>>> taskList2 = new List<Task<List<Temperature>>>();
            List<Task<List<Employee>>> taskList3 = new List<Task<List<Employee>>>();

            Stopwatch stopWatch = new Stopwatch();

            for (int x = 0; x < 100; x++)
            {
               taskList1.Clear();
               taskList2.Clear();
               taskList3.Clear();

               stopWatch = new Stopwatch();

               //STARTING STOPWATCH
               stopWatch.Start();

               //CREATE A LIST OF 1000 TASK
               for (int i = 0; i < 1000; i++)
               {
                  var roomId = new Random().Next(1, 10);
                  taskList1.Add(Bus.PostAsync(new Temperature() { HumidityValue = new Random().Next(45, 65), TemperatureValue = new Random().Next(15, 25), RoomId = roomId }));
                  taskList2.Add(Bus.GetListAsync<Temperature>(new Dictionary<string, object> { { "roomId", roomId } }));
                  taskList3.Add(Bus.GetListAsync<Employee>(new Dictionary<string, object> { { "roomId", roomId }, { "preference", null }, { "role", null } }));
               }

               //WAITING ALL TASKS
               Task.WaitAll(taskList1.ToArray());
               Task.WaitAll(taskList2.ToArray());
               Task.WaitAll(taskList3.ToArray());

               //STOPPING STOPWATCH
               stopWatch.Stop();
               string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", stopWatch.Elapsed.Hours, stopWatch.Elapsed.Minutes, stopWatch.Elapsed.Seconds, stopWatch.Elapsed.Milliseconds / 10);

               Console.WriteLine((x + 1) + "/100, Elapsed Time: " + elapsedTime);
            }

            Console.WriteLine("StressTest completed with success!");
            Console.ReadLine();
         }
      }
   }
}
